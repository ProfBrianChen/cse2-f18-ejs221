//Ethan Stein
//CSE 002 
//Professor Carr
//11-13-18
//Shuffle a card deck and return a hand with a user specified number of cards
import java.util.*; //import
public class shuffle{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in);//name scanner 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};   //name suits 
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};//specify cards 
String[] cards = new String[52]; //declare card array
String[] hand = new String[5]; //declare hand array
int numCards = 0; //initialize number of cards in a hand
int again = 1; //ensure loop runs
int index = 52;//set index of cards to 52
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; //fill out array for cards
} 
System.out.println(); //new line
printArray(cards, index); //call print array
shuffle(cards); //call shuffle array
printArray(cards, index); //call print array
index--;//reduce index by one to adjust for printing arrays
System.out.println();
System.out.println("Shuffled");
System.out.println("Enter how many cards in a hand");
numCards = scan.nextInt();
while(again == 1){ 
   hand = getHand(numCards, index, cards); //call get hand
   printArray(hand, numCards); //print hand
   index = index - numCards; //ajust the index
   System.out.println(" ");//new line
   System.out.println("Enter a 1 if you want another hand drawn"); //prompt user
   again = scan.nextInt(); //recieve input
}  
  } 

public static void shuffle(String[] cards) {
	System.out.println(" "); //new line
	
	Random rand = new Random();//name random num generator
	String converter = " ";//initialize converter
	int a = 0; //initialize variable for position in cards
	
	for (int i = 0; i < 200; i++) {
		converter = " "; //set converter to blank
		a = rand.nextInt(51) + 1;// set a to a random number
		converter = cards[0]; //assign converter to the first value of cards[]
		cards[0] = cards[a]; //swap random number in cards with the first
		cards[a] = converter; //reset position a to the initial value
	}
}

public static void printArray(String[] cards,int index) {
	for(int i = 0; i < index; i++) {
		System.out.print(cards[i]+" "); //print all elemnts of array cards
	}
}

public static String[] getHand(int numCards, int index, String[] cards) {
	String[] hand = new String[numCards]; //create new array for hand
	
	
	for(int i = 0; i < numCards; i++) {
		hand[i] = cards[index]; //assign the last (numCards) to hand
		index--;
	}
	
	
	return hand;//return hand
}

}



