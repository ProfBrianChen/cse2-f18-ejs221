//Ethan Stein
//CSE 002
//hw05
//Professor Carr
//October 9th, 2018
//Calculating probabilities of poker hands via simulations
import java.util.Scanner; //import scanner
import java.util.Random; //import random generator
public class hw05 {

	public static void main(String[] args) {
		Scanner scnr = new Scanner(System.in); //name scanner
		Random randGen = new Random(); //naming and declaring our random number
	    System.out.println("Input number of hands"); //prompt user for number of hands
	    //test to make sure they have inputed a user
	    //use a while statement that is active when a user inputs something other than an integer 
	    while (!scnr.hasNextInt()) {
	        System.out.println("That's not an integer, please try again.");//prompt user with error message
	        scnr.next(); //eliminates the error from the input
	    }
	    double handsNumber = scnr.nextInt(); //Receive input from user for how many number of hands
	    double printedHands = handsNumber;//use printedHands as a manipulable variable for the large loop
	    int card1 = 0; //initialize the first card
	    int card2 = 0;//initialize the second card
	    int card3 = 0;//initialize the third card
	    int card4 = 0;//initialize the fourth card
	    int card5 = 0;//initialize the fifth card
	    int aCard1 = 0;//initialize the adjust card 1 variable (ensure it is between 1 and 13)
		  int aCard2 = 0;//initialize the adjust card 2 variable (ensure it is between 1 and 13)
		  int aCard3 = 0;//initialize the adjust card 3 variable (ensure it is between 1 and 13)
		  int aCard4 = 0;//initialize the adjust card 4 variable (ensure it is between 1 and 13)
		  int aCard5 = 0;//initialize the adjust card 5 variable (ensure it is between 1 and 13)
	    int numAces = 0;//initialize the variable for number of aces in a hand
	    int num2 = 0;//initialize the variable for number of 2s in a hand
	    int num3 = 0;//initialize the variable for number of 3s in a hand
	    int num4 = 0;//initialize the variable for number of 4s in a hand
	    int num5 = 0;//initialize the variable for number of 5s in a hand
	    int num6 = 0;//initialize the variable for number of 6s in a hand
	    int num7 = 0;//initialize the variable for number of 7s in a hand
	    int num8 = 0;//initialize the variable for number of 8s in a hand
	    int num9 = 0;//initialize the variable for number of 9s in a hand
	    int num10 = 0;//initialize the variable for number of 10s in a hand
	    int numJack = 0;//initialize the variable for number of Jacks in a hand
	    int numQueen = 0;//initialize the variable for number of Queens in a hand
	    int numKing = 0;//initialize the variable for number of Kings in a hand
	    double numPairs = 0;//initialize the variable for number of pairs in a hand
	    double twoPair = 0;//initialize the variable for number of two pairs in the simulation
	    double threeOfKind = 0;//initialize the variable for number of three of a kinds in the simulation
	    double fourOfkind = 0; //initialize the variable for number of four of a kinds in the simulation
	    double numOnePair = 0;//initialize the variable for number of one pairs in the simulation
	    double probOnePair = 0.000; //initialize the probabilities of a one pair
	    double probTwoPair = 0.000; //initialize the probabilities of a two pair
	    double probThreeOfKind = 0.000; //initialize the probabilities of a three of a kind
	    double probFourOfKind = 0.000; //initialize the probabilities of a four of a kind
	
	/*Below is the for loop that will randomly generate the numbers, determine what card they are,
	 * calculate how many one pairs, two pairs, and 3 and 4 of kinds there are.
	 * After it will calculate store the number of them in a relevant variable
	 * 
	 */
	for (printedHands = handsNumber; printedHands > 0; --printedHands) {
		//lines 58-71 ensure that the number of each card is returned to 0 before each hand
	    numAces = 0; 
		num2 = 0;
	    num3 = 0;
	    num4 = 0;
	    num5 = 0;
	    num6 = 0;
	    num7 = 0;
	    num8 = 0;
	    num9 = 0;
	    num10 = 0;
	    numJack = 0;
	    numQueen = 0;
	    numKing = 0;
	    numPairs = 0;

		 card1 = randGen.nextInt(52) + 1; //randomly generates card one
		 card2 = randGen.nextInt(52) + 1;//randomly generates card two
		 
		 //checks to see if card1 and card 2 are the same, then generates card 2 on a loop until they are different
		 while (card1 == card2) {
			 card2 = randGen.nextInt(52) + 1;
		 }
		 
		 
		 card3 = randGen.nextInt(52) + 1;//randomly generates card 3
		 
		 //checks to see if card 3 matches any of the previous cards and then generates until it does not
		 while (card3 == card2 || card3 == card1) {
			 card3 = randGen.nextInt(52) + 1;
		 }
		 
		 card4 = randGen.nextInt(52) + 1; //randomly generates card 4

		//checks to see if card 4 matches any of the previous cards and then generates until it does not
		 while (card4 == card2 || card4 == card1 || card4 == card3) {
			 card4 = randGen.nextInt(52) + 1;
		 }
		 
		 
		 card5 = randGen.nextInt(52) + 1; //randomly generates card 5
		 
		//checks to see if card 5 matches any of the previous cards and then generates until it does not
		 while (card5 == card2 || card5 == card1 || card5 == card3 || card5 == card4) {
			 card5 = randGen.nextInt(52) + 1;
		 }
		 
		  //lines 105-109 adjusts each card to have a number between 1 and 13
		 //however, the numbers will still match the card (i.e. a 2 is still a 2 regardless of the suit)
		  aCard1 = card1 % 13 + 1;
		  aCard2 = card2 % 13 + 1;
		  aCard3 = card3 % 13 + 1;
		  aCard4 = card4 % 13 + 1;
		  aCard5 = card5 % 13 + 1;
		  
//the next 5 switch statements count the number of each card in a hand
//if the card generated is an ace, it will add one to the number of aces in the hand		   
//each statement does this for each possible card drawn 		  
switch (aCard1) { 
case 1: numAces++; break; //if card1 is an ace, add one to relevant variable
case 2: num2++; break;	//if card1 is an 2, add one to relevant variable	
case 3: num3++; break;//if card1 is an 3, add one to relevant variable
case 4: num4++; break;//if card1 is an 4, add one to relevant variable
case 5: num5++; break;//if card1 is an 5, add one to relevant variable
case 6: num6++; break;//if card1 is an 6, add one to relevant variable
case 7: num7++; break;//if card1 is an 7, add one to relevant variable
case 8: num8++; break;//if card1 is an 8, add one to relevant variable
case 9: num9++; break;//if card1 is an 9, add one to relevant variable
case 10: num10++; break;//if card1 is an 10, add one to relevant variable
case 11: numJack++; break;//if card1 is an Jack, add one to relevant variable
case 12: numQueen++; break;//if card1 is an Queen, add one to relevant variable
case 13: numKing++; break;//if card1 is an Kind, add one to relevant variable
}
//do the same as lines 115-128, but for card 2
switch (aCard2) {
case 1: numAces++; break;
case 2: num2++; break;		
case 3: num3++; break;
case 4: num4++; break;
case 5: num5++; break;
case 6: num6++; break;
case 7: num7++; break;
case 8: num8++; break;
case 9: num9++; break;
case 10: num10++; break;
case 11: numJack++; break;
case 12: numQueen++; break;
case 13: numKing++; break;
}
//do the same as lines 115-128, but for card 3
switch (aCard3) {
case 1: numAces++; break;
case 2: num2++; break;		
case 3: num3++; break;
case 4: num4++; break;
case 5: num5++; break;
case 6: num6++; break;
case 7: num7++; break;
case 8: num8++; break;
case 9: num9++; break;
case 10: num10++; break;
case 11: numJack++; break;
case 12: numQueen++; break;
case 13: numKing++; break;
}
//do the same as lines 115-128, but for card 4
switch (aCard4) {
case 1: numAces++; break;
case 2: num2++; break;		
case 3: num3++; break;
case 4: num4++; break;
case 5: num5++; break;
case 6: num6++; break;
case 7: num7++; break;
case 8: num8++; break;
case 9: num9++; break;
case 10: num10++; break;
case 11: numJack++; break;
case 12: numQueen++; break;
case 13: numKing++; break;
}
//do the same as lines 115-128, but for card 5
switch (aCard5) {
case 1: numAces++; break;
case 2: num2++; break;		
case 3: num3++; break;
case 4: num4++; break;
case 5: num5++; break;
case 6: num6++; break;
case 7: num7++; break;
case 8: num8++; break;
case 9: num9++; break;
case 10: num10++; break;
case 11: numJack++; break;
case 12: numQueen++; break;
case 13: numKing++; break;
}

//now we check for pairs for a given hand
//note: this is not checking for one pairs or two pairs, simply how many pairs, 0-2 in a hand
	//since each hand can have more the one pairs, we can not use else if statements
//each statement checks to see if there are two of a card, and if there are, increases the variable for number of pairs by 1
if (numAces == 2) {
	numPairs++;
}
if (num2 == 2) {
	numPairs++;
}
if (num3 == 2) {
	numPairs++;
}
if (num4 == 2) {
	numPairs++;
}
if (num5 == 2) {
	numPairs++;
}
if (num6 == 2) {
	numPairs++;
}
if (num7 == 2) {
	numPairs++;
}
if (num8 == 2) {
	numPairs++;
}
if (num9 == 2) {
	numPairs++;
}
if (num10 == 2) {
	numPairs++;
}
if (numJack == 2) {
	numPairs++;
}
if (numQueen == 2) {
	numPairs++;
}
if (numKing == 2) {
	numPairs++;
}

//now we are checking for 3 of a kind
//if there are 3 of a given card, it adds one to the total counter of three of a kind
if (numAces == 3) {
	threeOfKind++;
}
else if (num2 == 3) {
	threeOfKind++;
}
else if (num3 == 3) {
	threeOfKind++;
}
else if (num4 == 3) {
	threeOfKind++;
}else if (num5 == 3) {
	threeOfKind++;
}
else if (num6 == 3) {
	threeOfKind++;
}
else if (num7 == 3) {
	threeOfKind++;
}
else if (num8 == 3) {
	threeOfKind++;
}
else if (num9 == 3) {
	threeOfKind++;
}
else if (num10 == 3) {
	threeOfKind++;
}
else if (numJack == 3) {
	threeOfKind++;
}
else if (numQueen == 3) {
	threeOfKind++;
}
else if (numKing == 3) {
	threeOfKind++;
}

//now we check for 4 of a kind
//if there are 4 of any given card, add one two the total amount of four of a kinds
if (numAces == 4) {
	 fourOfkind++;
}
else if (num2 == 4) {
	fourOfkind++;
}
else if (num3 == 4) {
	fourOfkind++;
}
else if (num4 == 4) {
	fourOfkind++;
}else if (num5 == 4) {
	fourOfkind++;
}
else if (num6 == 4) {
	fourOfkind++;
}
else if (num7 == 4) {
	fourOfkind++;
}
else if (num8 == 4) {
	fourOfkind++;
}
else if (num9 == 4) {
	fourOfkind++;
}
else if (num10 == 4) {
	fourOfkind++;
}
else if (numJack == 4) {
	fourOfkind++;
}
else if (numQueen == 4) {
	fourOfkind++;
}
else if (numKing == 4) {
	fourOfkind++;
}

//this next if statements checks to see if there are two pairs in a hand
//if so, it adds one to the total counter of twoPairs
if (numPairs == 2) {
	twoPair++;
}
//if the number of pairs in a hand is one, it adds one to the total number of one pairs
else if(numPairs == 1) {
	numOnePair++;
}


	}//closes large for statement

//now that we have finished the large for statement, we have all the date we need to compute the output
//below we calculate the probability of each hand occurring given that information we just calculated
	//we do this by dividing the number of each outcome by the total number of each outcomes 
probOnePair = numOnePair/handsNumber;
probTwoPair = twoPair/handsNumber;
probThreeOfKind = threeOfKind/handsNumber;
probFourOfKind = fourOfkind/handsNumber;

//now we ensure that each probability is rounded properly (up or down)
if ((probOnePair * 10000) % 10 >= 5) { //check to see if the 4th decimal place is above or below 5
	probOnePair += .0005;  //if it is, add 5 to it to bring it above one
}
//now we adjust it to 3 decimal places by multiplying by 1000
//converting from double to an int and then back again and then diving by 1000
int converter = (int) (probOnePair * 1000);
probOnePair = (double) converter * .001;

//this process is repeated for all probabilities
if ((probTwoPair * 10000) % 10 >= 5) {
	probTwoPair += .0005;
}
converter = (int) (probTwoPair * 1000);
probTwoPair = (double) converter * .001;

if ((probThreeOfKind * 10000) % 10 >= 5) {
	probThreeOfKind += .0005;
}
converter = (int) (probThreeOfKind * 1000);
probThreeOfKind = (double) converter * .001;

if ((probFourOfKind * 10000) % 10 >= 5) {
	probFourOfKind += .0005;
}
converter = (int) (probFourOfKind * 1000);
probFourOfKind = (double) converter * .001;

//now that we have all the probabilities, we print them out 
System.out.println("Number of loops: " + handsNumber);
System.out.println("Probability of four of a kind: " + probFourOfKind);
System.out.println("Probability of three of a kind: " + probThreeOfKind);
System.out.println("Probability of a two pair: " + probTwoPair);
System.out.println("Probability of one pair: " + probOnePair);
		
		
	}
	}
