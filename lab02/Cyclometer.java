// Ethan Stein
// September 7th, 2018
//CSE 002 Professor Carr
// Record how far and fast a bicycle is traveling

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      // our input data. Document your variables by placing your


	   	int secsTrip1=480;  // how many seconds in trip 1
       	int secsTrip2=3220;  // how many seconds are in trip 2
		int countsTrip1=1561;  // how many wheel rotations in trip 1
		int countsTrip2=9037; // how many wheel rotations in trip 2
      double wheelDiameter=27.0,  //the diameter of the wheel(used for determing distance)
  	PI=3.14159, //math constant pi
  	feetPerMile=5280,  // how many feet are in a mile
  	inchesPerFoot=12,   // how many inches in a foot
  	secondsPerMinute=60;  // how many seconds in a minute
	double distanceTrip1, distanceTrip2,totalDistance;  //variables used to calculate the distance travelled
      
        System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      
    //run the calculations; store the values. Document your
		//calculation here. What are you calculating?
		// Trip 1 took 8.0 minutes and had 1561 counts.
		// Trip 2 took 53.666666666666664 minutes and had 9037 counts.
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // computes the distance travelled in trip 2 
	totalDistance=distanceTrip1+distanceTrip2; //simply adds the trips of the distance together 

  System.out.println("Trip 1 was "+distanceTrip1+" miles"); //this prints out the distance travelled in trip 1
	System.out.println("Trip 2 was "+distanceTrip2+" miles"); //this prints out the distance travelled in trip 2
	System.out.println("The total distance was "+totalDistance+" miles"); //this prints out the distance travelled in both trips



	}  //end of main method   
} //end of class