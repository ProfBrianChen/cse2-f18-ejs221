//Ethan Stein
//CSE 002
//Professor Carr/
//October 11th, 2018
//Lab 05, ensuring that inputs are in the correct form.
import java.util.Scanner;//import scanner
public class lab05 {
  
  public static void main(String[] args){
 Scanner sc = new Scanner(System.in); //name scanner
boolean loop = false;//boolean variable that is used to automatically start certain loops until broken
int courseNum = 0; //initialize var for number of courses
int meetWeek = 0;//initialize var for number of weeks
int timeStart = 0;//initialize var for time class starts
int numStudents = 0;//initialize var starts


while(loop == false) { //ensuring that loop is automatically triggered
	System.out.println("Please enter a course number!"); //prompt user
while (!sc.hasNextInt()) { //checking if the computer did not recieve an int input
        System.out.println("That's not a course number! Try again!");//informing user of their silly error
     sc.next(); //removing error from input
    }

courseNum = sc.nextInt(); //assigning input to variable 
if (courseNum <= 0) { //checking to see if input was negative
	 System.out.println("Negatives are not valid");
	continue; //returning user to the beginning of the statement
}
break;//breaking out of the while statement
}//ending the big while statement

System.out.println("Please enter a department name"); //prompting user for dept name
while (sc.hasNextDouble() || sc.hasNextInt() ) { //checking to make sure its not a number
	System.out.println("Please enter a department name, not a number"); //informing them of error
	sc.next();//removing error from input
}
String deptName = sc.next();//assigning input to variable 

System.out.println("Enter how many meetings per week");//prompting user for meetings per week
while(!sc.hasNextInt()) {//checking to see if input is an not an int
	System.out.println("Sorry but that is not a valid entry, please try again");//informing them of error
	sc.next();//removing error from input
}
meetWeek = sc.nextInt();//assigning input to variable 

while(loop == false) {//ensuring that loop is automatically triggered
System.out.println("What time does the class start?");//prompt user for input
while(!sc.hasNextInt()) {//checking if input is not an int 
	System.out.println("Sorry but that is not a valid entry, please try again");//informing them of error
	sc.next();//removing error from input
}	
timeStart = sc.nextInt();//assigning input to variable 
if(timeStart < 0 || timeStart >= 24) { //checking if input is not in between 0-24 (military time)
	System.out.println("This is not a valid time.");//informing them of error
	continue;//looping them back to the start of the time loop to give them a chance to get it right
}
break;//breaking bigger while loop 
}

while(loop == false) {//ensuring that loop is automatically triggered
System.out.println("How many kids are in the class?");
while(!sc.hasNextInt()) {//checking if input is not an int 
	System.out.println("Sorry but that is not a valid entry, please try again");//informing user of error
	sc.next();//removing error from input
}	
numStudents = sc.nextInt();//assigning input to variable 
if(numStudents < 0) {//checking to see if number is negative
	System.out.println("This is not a valid input");//informing them of error
	continue;//looping them to start of the bigger loop
}
break;//breaking bigger loop
}

System.out.println("Please enter the teacher name");//prompt user to enter teacher name
while (sc.hasNextDouble() || sc.hasNextInt() ) {//checking to see if they entered a word
	System.out.println("Please enter the teacher name, not a number");//informing them of error
	sc.next(); //removing error from input
}
String teachName = sc.next();//assigning input to variable 

//printing out relevant info along with descriptions
System.out.println("Course: " + deptName + " " + courseNum);
System.out.println("Tought by Professor " + teachName);
System.out.println("Class starts at " + timeStart + " and meets " + meetWeek + " times a week");
System.out.println("There should be " + numStudents + " students in the class");



  }  
}

