//Ethan Stein
//Professor Carr
//CSE 002
//September 27th, 2018
//Generate random cards
import java.util.Scanner;//import scanner
import java.util.Random;//import random generator
public class CrapsIf {
  
  public static void main(String[] args){
 Random randGen = new Random();
    Scanner myScanner = new Scanner( System.in );
    String random = "blank"; //introduce string
    int die1 = 0; //introduce die 1
    int die2 = 0;//introduce die 2
    
   System.out.println("Would you like random dice? [yes/no]"); //ask user if they want random die
    random = myScanner.nextLine(); //recieve input
    
    //now we are either going to have them input the die or have them be random
    if (random.equals("yes")){ //if they want random die, randomly chose numbers between 1 and 6
       die1 = randGen.nextInt(6) + 1; 
      die2 = randGen.nextInt(6) + 1;
    }
    
    else if (random.equals("no")){ //if they dont, ask them what numbers they want
     System.out.println("input die one"); // ask for die one
      die1 = myScanner.nextInt(); //recieve input
      
      System.out.println("input die two");// ask for die two
      die2 = myScanner.nextInt();//recieve input
    }
                   
   
   //now we have to input all the slang terms and situations in which they are selected 
    if (die1 + die2 == 2){
      System.out.println("Snake Eyes"); //if both die are one
    }
    
    else if (die1 + die2 == 3){
      System.out.println("Ace Deuce"); //if one die is one and the other is 2
    }
    
    else if (die1 + die2 == 4 && die1 != 2){
      System.out.println("Easy Four"); //if one die is one and the other is 3 
    }
    
     else if (die1 + die2 == 4 && die1 == 2){
      System.out.println("Hard Four"); //if both die are 2
    }
    
    else if (die1 + die2 == 5){
      System.out.println("Fever Five"); //if the die add up to 5
    }
    
    else if (die1 + die2 == 6 && die1 != 3){
      System.out.println("Easy Six"); //if the die add up to 6 but the die are not the same number
    }
    else if (die1 + die2 == 6 && die1 == 3){ //if both die are 3
      System.out.println("Hard Six"); 
    }
    
    else if (die1 + die2 == 7){
      System.out.println("Seven out"); //if the die add up to 7
    }
    
    else if(die1 + die2 == 8 && die1 != 4){
      System.out.println("Easy Eight"); //if the die add up to 8 but are not 4
    }
    
       else if(die1 + die2 == 8 && die1 == 4){ //if both die are 4
      System.out.println("Hard Eight");
    }
    
    else if (die1 + die2 == 9){
       System.out.println("Nine"); //if the die add up to nine
    }
    
     else if(die1 + die2 == 10 && die1 != 5){
      System.out.println("Easy Ten"); //if the die add up to 10 but are not 5
    }
    
     else if(die1 + die2 == 10 && die1 == 5){ //if both die are 5
      System.out.println("Hard Ten");
    }
    
     else if (die1 + die2 == 11){
       System.out.println("Yo-leven"); //if the die add up to 11
    }
    
      else if (die1 + die2 == 12){
       System.out.println("Box Cars"); //if the die add up to 12
    }
    
    else {
      System.out.println("There has been a mistake! Please try again!"); //if the user inputs incorrect numbers
    }
    
    
    
     
                      
  }  
}