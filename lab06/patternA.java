
import java.util.Scanner;
public class patternA {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num = 0;
		int counter = 0;
		boolean loop = false;
		while(loop == false) {
			System.out.println("Enter a number between 1 and 10");
		 while (!sc.hasNextInt()) {
		        System.out.println("That's not an integer, please try again.");//prompt user with error message
		        sc.next(); //eliminates the error from the input 
		    }
		 num = sc.nextInt();
		 if (num >= 1 && num <= 10) {    
		        break;
		    }
		 else {
			 System.out.print("Sorry but this is incorrect. ");
			 continue;
		 }
		 }
	
		
		for(int i = 0; i <= num; i++)
		{
			for(int j = 1; j <= i; j++)
			{
					System.out.print(j + " ");
			}
			System.out.println(" ");
		}
		
	}

}
