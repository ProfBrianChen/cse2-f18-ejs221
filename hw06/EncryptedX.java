import java.util.Scanner; //import scanner
public class EncryptedX { 
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); //name scanner
		int size = 0; //initialize variable to size of 
		boolean loop = false;
		while(loop == false) {
			System.out.println("Enter a number between 1 and 100");
		 while (!sc.hasNextInt()) { //check to make sure if input is a number
		        System.out.println("That's not an integer, please try again.");//prompt user with error message
		        sc.next(); //eliminates the error from the input 
		    }
		 size = sc.nextInt(); //assign user input to variable
		 if (size >= 0 && size <= 100) { //check if variable is in range    
		        break; //if it is, end the loop 
		    }
		 else {
			 System.out.print("Sorry but this is incorrect. "); //inform user of their silliness
			 continue; //begin loop again if their is an error
		 }
	}
for (int counter = 1; counter < size; counter++) {//loop that outputs lines
	System.out.println("");//create a new line
	for (int rows = 1; rows < size; rows++) { //loop that outputs a line of stars
		
		if (rows == counter || rows == (size - counter)) {
		System.out.print(" "); //inputs a space instead of a * when necessary
		}
		
		else {
			System.out.print("*");//if not, print a star in the loop
		}//end else
		
		
	}//end small for loop
	
}//end big for loop
		
		
	}
}