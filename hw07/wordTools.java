
import java.util.Scanner;//import scanner
public class wordTools {
	static Scanner scnr = new Scanner(System.in); //import and name scanner in the class
	public static void main(String[] args) {
		boolean bigLoop = true;//set loop for entire code to active
		boolean loop = true;//set code for loop that ensure entry is correct to active
		int numNonWS = 0; //Initialize variable for number of non white space characters
		int numWords = 0; //Initialize variable for number
		int numOccur = 0;//Initialize variable for number of times a phrase occurs
		String phraseToFind = " ";//Initialize string variable for phrase to be found
		String menuOption = " ";//Initialize string variable for the menu option selected by user
		String newText = " "; //Initialize string variable for replacing ! with . and shortening spaces
	String text = sample();//call sample method
	
	while(bigLoop == true) { //ensure loop for program auto activates
	System.out.println("Enter an option for the menu, ensure you enter a valid response");//prompt user for menu option
	while (loop == true) {//ensure loop for ensuring correct inputs auto activates
		menuOption = printMenu(); //set menu option to result from printMenu method
if (menuOption.equals("c") || menuOption.equals("w") || menuOption.equals("f") || menuOption.equals("r") || menuOption.equals("s") || menuOption.equals("q") ) {
	break; //if input is vaid, break loop
}
else {
	System.out.println("Please enter a valid input"); //if input is invalid, tell user
	continue; //continue loop
}
} 

	if (menuOption.equals("c")) {
		numNonWS = numNonWS(text); //if is selected, run numNonWS method and assign to variable
		System.out.println("Number of characters (not including spaces): " + numNonWS); // print result
	}
	
	else if (menuOption.equals("w")) {
		numWords = numWords(text);//if selected run numWords method and assign to numWords
		System.out.println("Number of words: " + numWords);//print result
	}
	
	else if (menuOption.equals("f")) {
		System.out.println("Which phrase would you like to find");//prompt user for phrase to find
		phraseToFind = scnr.nextLine(); //assign to variable
		numOccur = findPhrase(text, phraseToFind); //running the findPhrase method and assigning result to variable
		System.out.println("Number of times phrase occured: " + numOccur);//returning answer
	}
	else if (menuOption.equals("r")) {
		newText = replaceEx(text);//running replace exclamation points method and assigning adjust text to variable
		System.out.println(newText);//printing it
	}
	else if (menuOption.equals("s")) {
		newText = shortenSpace(text);//running shortenSpace method and assigning string result to method
		System.out.println(newText);//displaying result
	}
	else if (menuOption.equals("q")) {
		System.out.println("Thank you for using this program");//signing off 
		bigLoop = quit(); //running quit method
	}
	
}
	}
	public static String sample() {
		System.out.println("Enter text"); //prompt user for text
		String text = scnr.nextLine(); //save test as string
		System.out.println("Your text is: " + text); //display
		return text; //return text to main method
	}
	
	public static String printMenu() {
		String menuOption = " ";//Initialize menuOption
		//shower user options
		System.out.println("c - Number of non-whitesapce characters");
		System.out.println("w - Number of words");
		System.out.println("f - find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");
		menuOption = scnr.nextLine(); //Receive input from user for menu choice
		
		return menuOption;//return menuOption to main method
 
	}
	
	public static int numNonWS(String text) {
		int numNonWS = 0; //set number of non white space characters to 0
		for (int count = 0; count < text.length(); ++count) { //set a for loop that will check each character to see if its a not a space
		if (text.charAt(count) != ' ') {//if statement checking if a character is not a space
			++numNonWS;//add 1 to number of non white space characters
		}// end if
		}//end for
		return numNonWS;//return integer to main method
	}//end numNonWS
	
	public static int numWords(String text) {
		int numWords = 1; //set  number of words to 1 so count the last 
		for (int count = 0; count < text.length(); ++count) { //run loop counting spaces
		if (text.charAt(count) == ' ' && text.charAt(count + 1) != ' ') {//checks to see if a character is a space without one next to it
			++numWords;//adds one to counter of words
		}//end if
		}//end for
		return numWords;//return to main method
	}
	
	public static int findPhrase(String text, String phraseToFind) {
		int numOccur = 0;//integer for number of times a phrase occurs
		while(text.contains(phraseToFind)) { //loop if phrase is in code
		if(text.contains(phraseToFind)) {//if phrase is in the code, delete it
			text = text.replaceFirst(phraseToFind, "");
			numOccur++; //add one to counter for number of time phrases occurs
		}
		}
		return numOccur; //return number of deletions
	}
	
	public static String replaceEx(String text){
			if(text.contains("!")) { //check to see if ! exists
				text = text.replace("!", "."); //if it does, replace it with .
		}
		return text; //return the adjusted text
	}
	
	public static String shortenSpace(String text){
			if(text.contains(" ")) { //see if text contains a space
				text = text.replaceAll("( )+", " "); //if so, convert all spaces to single spaces
			}
		return text; //return adjust text
	}
	public static boolean quit() {
		boolean bigLoop = false; //set big loop variable to false
		return bigLoop;//return the boolean
	}
	
}