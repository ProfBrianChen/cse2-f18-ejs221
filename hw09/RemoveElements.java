//Ethan Stein
//CSE 02
//Professor Carr
//11-27-18
//Removing elements from arrays
import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.println("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
 public static int[] randomInput() {
	 Random rnd = new Random(); //call random
	 int list[] = new int[10]; 
		int a = 0;
		int count = 0;
		//assign random numbers to each index in the variable
		while (count < 10) {
		a = rnd.nextInt(10);
		list[count] = a;
		count++;
		}
return list; //return array

 }
 
  
  public static int[] delete(int list[], int pos) {
	int count = 0;
	int temp = 0;
	int array1[] = new int[9]; //declare new array with one less number
	while (temp < 9) {
		if (pos == count) {
			count++;
			continue; //if position matches index, add to count and go back to start of loop
		}
		else if (pos != count) {
			array1[temp] = list[count]; //if pos does not max the index, keep counting and assigning to new array
		}
		count++; //add to count
		temp++;//add to array
		
		//not: temp is the index in the new array after the number has been deleted
	}
	return array1;  
  }
  
  public static int[] remove(int list[], int target) {
	 int occurences = 0;
	  for (int count = 0; count < list.length; count++) {
		  if (list[count] == target) {
			 occurences++; //see how many occurences there are using linear search
		  } 
	  }
	  
	 int[] newArray = new int[10 - occurences]; //subtract the number of occurences from the length of the initial array
	 //int newLength = newArray.length;
	 int counter = 0; //declare counter and x which is the counter that gets adjusted for based on if a number gets removed 
	 int x = 0;
	 
	 for (int count = 0; count < list.length; count++) {
		 if (list[count] != target) {
			 newArray[x] = list[count]; //if target does not match number, assign to new array
			 x++;
		 }
		 
		 if (list[count] == target) {
			 continue; //if the target matches the number, go back to top of loop and dont assign it to new array
		 }
	 }
	 
	return newArray; //return new array
  }
}
