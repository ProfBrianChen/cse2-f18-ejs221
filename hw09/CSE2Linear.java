//Ethan Stein
//CSE 02
//Professor Carr
//11-27-18
//Using binear and linear searches to search for grade scores
import java.util.*;
public class CSE2Linear {
public static void main(String[] args) {
Scanner scnr = new Scanner(System.in);
System.out.println("Enter 15 ascending grades for students, Each score should be an int between 1-100");
int score[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //initialize score

int count = 0; //initialize counter

//while loop promps user to enter 15 scores in ascending order
while (count < 15) {
	if (scnr.hasNextInt() == false) {
		System.out.println("Please enter an integer"); //reminds user to enter enter and checks to see if one is entered
		scnr.next(); //removes incorrect input
		continue; //restarts loop
	}
	else {
		score[count] = scnr.nextInt(); //recieves input
		//following if statements checks to see if input is out of range and then adjusts for mistake
		if (score[count] >= 100 || score[count] <= 0) {
			System.out.println("please enter an integer between 1-100");
			count--;
		}
		
		//these make sure integers are in ascending order
		if (count != 0) { //makes sure this does not check for the first integer (since its 0)
		if (score[count] < score[count - 1]) {
			System.out.println("Please enter integers in ascending order");
			count--; 
		}
		}
		
}
count++;	//increaes count
}

for(int counter = 0; counter < 15; counter++) {
	System.out.println(score[counter]); //assigns 15 inputs to an array
}

boolean loop = true;
int searchScore = 0;
System.out.println("Enter a grade to be searched for"); //prompts user for a search
while (loop == true) {
	if (scnr.hasNextInt()) {
		searchScore = scnr.nextInt(); //recieves search if its an integers
		loop = false;
	}
	else {
		System.out.println("Error, enter an integer"); //informs user of mistake
		scnr.next();
	}
}
binarySearch(score, searchScore); //calls binary search method
scramble(score);
linearSearch(score, searchScore);

}

public static void scramble(int score[]) {
	Random rnd = new Random();//initialize random 
	int switcher = 0;
	int a = 0;
	
	//switch random numbers in the array with the first one
	for (int count = 0; count < 15; count++) {
		a = rnd.nextInt(15);
		switcher = score[count];
		score[count] = score[a];
		score[a] = switcher;
	}
}

public static void linearSearch(int score[], int search) {
	int iterations = 0;
	boolean found = false;
for (int count = 0; count < 15; count++) { //run through each index and see if it matches 
	if (score[count] == search) {
	found = true; //if found, break
	break;
}	
	iterations++;
}
if (found = true) {
System.out.println("The score was found with " + iterations + " iterations in a linear search"); //inform user of result
}
}


public static void binarySearch(int score[], int search) {
	int low = 0; //declares low of array
	int high = 14;//declares high of array
	int mid = (low + high) / 2;//declares mid of array
	int iterations = 0;//declare iterations
	boolean found = false;
	
	while (low  <= high) {
		
		
		if (score[mid] > search) {
			high = mid - 1; //checks to see if search is below the mid
		}
		else if (score[mid] < search) {
			low = mid + 1;//checks to see if search is above the mid
		}
		
		else if (score[mid] == search) {
			System.out.println("Found"); //checks to see if its found
			found = true;
			break;//breaks loop
		}
		
		mid = (low + high) / 2; //cuts array in half
		iterations++;
		
	}
	
	//inform user of result
	if (found == true) {
	System.out.println("Found in " + iterations + " iterations with linear search");
	}
	else {System.out.println("Not found in " + iterations + " iterations");}
	
}

}
