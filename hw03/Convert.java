// Ethan Stein
// September 17th, 2018
//CSE 002, Professor Carr
// Converting different types of units

import java.util.Scanner; // importing a scanner to recieve user input
public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in ); //naming the scanner we just imported
       
  System.out.println("Enter the area of the land affected by the hurricane in acres");
   double acres = myScanner.nextDouble(); //recieve the acres of the land
          
  System.out.println("Enter the rainfall in inches");
   double rain = myScanner.nextDouble(); //recieve the rainfall
          
  //now we find the volume of rain in cubic feet
  //one acre = 43560 square feet
  double squareFeetofRain = acres * 43560 * (rain/12); //multiplying square feeet by feet of rain
   double volumeRain = squareFeetofRain * 0.00000000000679357; // convert cubic feet to cubic mile
          
          System.out.println(volumeRain + " Cubic Miles of rain");
         
          
   

}  //end of main method   
  	} //end of class
