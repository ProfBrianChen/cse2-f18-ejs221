// Ethan Stein
// September 17th, 2018
//CSE 002, Professor Carr
// Converting different types of units
import java.util.Scanner;
public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {
 Scanner myScanner = new Scanner( System.in ); //naming the scanner we just imported
 
    System.out.println("Enter the width of the pyramid");
    double width = myScanner.nextDouble(); //recieve the width of the pyramid
          
          
    System.out.println("Enter the width of the pyramid");
    double height = myScanner.nextDouble(); //recieve the height of the pyramid
          
    //now we calculate the volume but squaring the width and multiplying by the height, then dividing by 3
    double volume = (width * width * height) / 3;
          
    System.out.println("The volume is " + volume);
          
         

}  //end of main method   
  	} //end of class