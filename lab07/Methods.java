import java.util.Scanner;
import java.util.Random;
public class Methods {
public static void main(String[] args){
	Scanner scnr = new Scanner(System.in); 
	boolean loop = true;
	while(loop == true) {
	System.out.println();
	
	String adjective = adjectives();
	String verb = vrb();
	String subject = subject();
	String object = object();
	String sent1 = "The " + adjective + " " + subject + " " + verb + " the " + object;
	System.out.println(sent1);
	sentences(subject, adjective, object);
	
	System.out.println(" ");
	System.out.println("Would you like another sentence? Type 1 for yes and 2 for no");
	int answer = scnr.nextInt();
	if (answer == 1) {
		continue;
	}
	else if (answer == 2) {
		System.out.println("goodbye");
		break;
	}
	else {
		System.out.println("sorry this is an incorrect input");
		
	}
		   }  
}
		 
public static String adjectives() {
	Random randGen1 = new Random();
	int rand1 = randGen1.nextInt(10);
	String word = " ";
	
	switch(rand1) {
	case 0: 
		word = "smart"; break;
	case 1: 
		word = "tall"; break;
	case 2: 
		word = "short"; break;
	case 3: 
		word = "heavy"; break;
	case 4:
		word = "dark"; break;
	case 5: 
		word = "light"; break;
	case 6: 
		word = "hot"; break;
	case 7: 
		word = "scary"; break;
	case 8: 
		word = "drippy"; break;
	case 9:
		word = "hot"; break;
		
	}
	return word;
	}

public static String vrb() {
	Random randGen3 = new Random();
	int rand3 = randGen3.nextInt(10);
	String word = " ";
	switch(rand3) {
	case 0: 
		word = "threw"; break;
	case 1: 
		word = "kicked"; break;
	case 2: 
		word = "broke"; break;
	case 3: 
		word = "gave"; break;
	case 4:
		word = "dropped"; break;
	case 5: 
		word = "held"; break;
	case 6: 
		word = "hid"; break;
	case 7: 
		word = "vaulted"; break;
	case 8: 
		word = "lost"; break;
	case 9:
		word = "dumped"; break;
		
	}
	return word;
	}
public static String object() {
	Random randGen4 = new Random();
	int rand4 = randGen4.nextInt(10);
	String word = " ";
	switch(rand4) {
	case 0: 
		word = "dog"; break;
	case 1: 
		word = "baby"; break;
	case 2: 
		word = "cup"; break;
	case 3: 
		word = "coffee"; break;
	case 4:
		word = "computer"; break;
	case 5: 
		word = "phone"; break;
	case 6: 
		word = "bag"; break;
	case 7: 
		word = "box"; break;
	case 8: 
		word = "rock"; break;
	case 9:
		word = "food"; break;
		
	}
	return word;
	}

public static String subject() {
	Random randGen2 = new Random();
	int rand2 = randGen2.nextInt(10);
	String word = " ";
	switch(rand2) {
	case 0: 
		word = "bottle"; break;
	case 1: 
		word = "man"; break;
	case 2: 
		word = "lake"; break;
	case 3: 
		word = "mountain"; break;
	case 4:
		word = "house"; break;
	case 5: 
		word = "fire"; break;
	case 6: 
		word = "duck"; break;
	case 7: 
		word = "table"; break;
	case 8: 
		word = "train"; break;
	case 9:
		word = "bubble"; break;
	}
	return word;
	}
  
  public static void sentences(String subject, String adjective, String object){
System.out.println("The " + subject + " felt too " + adjective);
System.out.println("The " + object + " did not want the " + subject + " to do that");
  }  
}