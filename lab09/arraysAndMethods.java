//Ethan Stein
//CSE 002
//Professor Carr
//11/16/18
//Using arrays in method arguments and inverting and copying them
public class arraysAndMethods {

public static void main(String[] args) {
		int[] array0 = {0,1,2,3,4,5,6,7};
		int[] array1 = copy(array0);
		int[] array2 = copy(array0);
	inverter(array0);
	print(array0);
	inverter2(array1);
	print(array1);
	int[] array3 = inverter2(array2);
	print(array3);
	
	}
public static void print(int[] Array) {
	for (int counter = 0; counter < Array.length; counter++) {
		System.out.print(Array[counter]);
	}
	System.out.println("");
}
	
public static int[] copy(int[] Array){
	int[] copiedArray = new int[Array.length];
	
	for (int counter = 0; counter < Array.length; counter++) {
		copiedArray[counter] = Array[counter];
	}
	return copiedArray;
}

public static void inverter(int[] Array) {
	int placeholder = 0;
	for(int counter = 0; counter < (Array.length/2); counter++) {
		placeholder = Array[counter];
	Array[counter] = Array[Array.length - counter - 1];
	Array[Array.length - counter - 1] = placeholder;
	}

}

public static int[] inverter2(int[] Array) {
int[] invertedArray = copy(Array);
inverter(invertedArray);
return invertedArray;
}


}
