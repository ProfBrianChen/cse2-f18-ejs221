//Ethan Stein
//Professor Carr
//CSE 002
//September 21st, 2018
//Generate random cards

import java.util.Random;// importing random number generator
public class CardGenerator {
  
  
  public static void main(String[] args){
Random randGen = new Random(); //naming and declaring our random number
 
 int card = randGen.nextInt(52) + 1;
String suit = "blank"; //naming the string for our suit
    //System.out.println(card);
  
    // for this if else statement, were going to determine the suit and then adjust the card variable to be between 1-13
    if (card <= 13) {
      suit = "Clubs"; //determining if clubs
    }
  else if (card >= 14 && card < 27) {
      suit = "Spades"; //determining if spades
    card = card - 13;  //making sure varibale is between 1-13
    }
    
    else if (card >= 27 && card < 40) {
      suit = "Hearts"; //determining if hearts
         card = card - 26;//making sure varibale is between 1-13
    }  
      else if (card >= 40 && card < 52) {
      suit = "Diamonds"; //determining if Diamonds
      card = card - 39;//making sure varibale is between 1-13
    }
    
    
    // now that we have suit and a number between 1-13, we can pick the final card
   if (card  < 11 && card != 1){
     System.out.println("You picked a " + card + " of " + suit); //if card is between 2-10, we print out the number
   }
      else if (card == 1){
        System.out.println("You picked an ace of " + suit); //if the card is 1, its an ace, so we print accordingly
      }
    
    else if (card == 11){
        System.out.println("You picked a jack of " + suit); //if the card is 11, its a jack, so we print accordingly
      }
  else if (card == 12){
        System.out.println("You picked a queen of " + suit); //if the card is 12, its a queen, so we print accordingly
      }
    else if (card == 13){
        System.out.println("You picked a king of " + suit); ////if the card is 13, its a king, so we print accordingly
      }
    
                      
  }  
}