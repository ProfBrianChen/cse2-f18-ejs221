// Ethan Stein
// September 11th, 2018
//CSE 002 Professor Carr
// Calculate various associated with a shopping purchase

public class Arithmetic{
  
  public static void main(String args[]) {
  

  
    

int numPants = 3;//number of pants
double pantsPrice = 34.98; //cost of pair of pants


int numShirts = 2; //Number of sweatshirts
double shirtPrice = 24.99;//Cost per shirt


int numBelts = 1; //Number of belts
double beltCost = 33.99; //cost per belt
double paSalesTax = 0.06; //the tax rate

//find total prices for all items    
double shirtsTotal; 
double pantsTotal; 
double beltsTotal; 
shirtsTotal = numShirts * shirtPrice;//cost of all shirts
pantsTotal = pantsPrice * numPants; //cost of all pants
beltsTotal = numBelts  * beltCost; //cost of all belts
    
    
//find tax on all times
double shirtsTax;
double pantsTax;
double beltsTax;
shirtsTax = shirtsTotal * paSalesTax; //finds total tax on shirts
pantsTax = pantsTotal * paSalesTax; //finds total tax on pants
beltsTax = beltsTotal * paSalesTax; //finds total tax on belts

    
//finding the cost of all the items together without tax
double totalCost; 
totalCost = numPants * pantsPrice + numShirts * shirtPrice + numBelts * beltCost; 
  
//find the tax on the entire purchase
double taxTotal;
taxTotal = totalCost * paSalesTax;
    
//find the total cost of the purchase, including tax
double totalTransaction;
totalTransaction = taxTotal + totalCost;
    
 //because we are dealing with money, we are now going make sure each price only has two decimal places before we display them
//we are going to use a converter integer to make this process easier


//conversion for tax on shirts    
shirtsTax *= 100;
int converter1 = (int) shirtsTax;
shirtsTax = converter1 * .01;
    
//conversion for tax on pants    
pantsTax *= 100;
int converter2 = (int) pantsTax;
pantsTax = converter2 * .01;

//conversion for tax on belts    
beltsTax *= 100;
int converter3 = (int) beltsTax;
beltsTax = converter3 / 100.00;

    
 //conversion for total sales tax
 taxTotal *= 100;
int converter4 = (int) taxTotal;
taxTotal = converter4 * .01;
 
//conversion for total transaction cost    
 totalTransaction *= 100;
int converter5 = (int) totalTransaction;
totalTransaction = converter5 * .01;

    
//now that we have done the calculations, we are going to display the results
//first display the total cost of the items and the text associated with the purchase
System.out.println("Total Cost of Shirts is $" + shirtsTotal + " plus $" + shirtsTax + " in tax"); //displays cost of shirts and how much tax 
System.out.println("Total Cost of Pants is $" + pantsTotal + " plus $" + pantsTax + " in tax"); //displays cost of pants and how much tax
System.out.println("Total Cost of Belts is $" + beltsTotal + " plus $" + beltsTax + " in tax"); //displays cost of shirts and how much tax 

//compute and display total costs 
System.out.println("Total cost of the purchase before tax is $" + totalCost); //displays total cost of purchase without tax
System.out.println("Total sales tax is $" + taxTotal); //displays tax of the whole purchase
System.out.println("The total transaction cost including tax is $" + totalTransaction); //displays total transaction cost
}
  
  
}


