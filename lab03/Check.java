// Ethan Stein
// September 14th, 2018
//CSE 002 Professor Carr
// USing input from the user to perform simple calculations

import java.util.Scanner; //importing a scanner, used to recieve user input

public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
 Scanner myScanner = new Scanner( System.in ); //now that weve imported the scanner, we have to name it 

          
System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompt user for cost of check
  double checkCost = myScanner.nextDouble(); //recieve the total cost of the check

System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " ); //prompt user for tip percentage
  double tipPercent = myScanner.nextDouble(); //recieve tip percentage
  tipPercent /= 100;
          
 System.out.print("Enter the number of people who went out to dinner:"); //prompt user for number of people paying
          int numPeople = myScanner.nextInt(); //recieve number of people paying
          
    
   double totalCost;
double costPerPerson;
int dollars, dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
totalCost = checkCost * (1 + tipPercent); //finding the total cost of the dinner including tips
costPerPerson = totalCost / numPeople; //dividing that cost amoung the number of people at dinner
//get the whole amount, dropping decimal fraction
dollars=(int)costPerPerson; //setting dollars equal to cost per person
//get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
dimes=(int)(costPerPerson * 10) % 10;  //finding the number of dimes/the tenths place
pennies=(int)(costPerPerson * 100) % 10; // finding the number of pennies/the one hundredth place
System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);




          
          
          
          
}  //end of main method   
  	} //end of class

