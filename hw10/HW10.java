//Ethan Stein
//CSE 002, Professor Carr
//December 4th, 2018
//Tic Tac Toe with multidimensional arrays
import java.util.*; //import scanner and other things
public class HW10 {

public static void main(String[] args) {
Scanner sc = new Scanner(System.in);//initialize scanner
char[][] grid = new char[3][3]; //initialize grid variable
int count = 0;//initialize move counter to 0
char team;//Initialize char to team
boolean gameOn = true; //initialize large gameOn loop
int input = 0; //Initialize input to 0
int[] numUsed = new int[9]; //initialize array that will contain all the inputs


//assign each entry in the grid to a char
grid[0][0] = '1';
grid[0][1] = '2';
grid[0][2] = '3';

grid[1][0] = '4';
grid[1][1] = '5';
grid[1][2] = '6';

grid[2][0] = '7';
grid[2][1] = '8';
grid[2][2] = '9';

print(grid); //call print method

while (gameOn == true) {
team = 'X';//change team to X
System.out.println("please enter an input for team X");
while (!sc.hasNextInt()) {
    System.out.println("That's not an integer, please try again.");//prompt user with error message
    sc.next(); //eliminates the error from the input
}
input = sc.nextInt(); //assign input to user input
input = check(input,count, numUsed);
numUsed[count] = input;
//System.out.println(input + "checks");
userInput(input,grid,team); //call user input method
win(grid, count); //call win method to see if they win
count++; //add one to move count


System.out.println("please enter an input for team 0");
while (!sc.hasNextInt()) {
    System.out.println("That's not an integer, please try again.");//prompt user with error message
    sc.next(); //eliminates the error from the input
}
team = 'O';//change team to O
input = sc.nextInt();//assign input to user input
input = check(input,count, numUsed);
numUsed[count] = input;
userInput(input,grid,team);//call user input method
win(grid, count);//call win method to see if they win
count++;//add one to move count


}
}



public static void print(char[][] grid) {
//method for printing grid
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++ ) {
	System.out.print(grid[i][j] + " ");//prints row
	}
	System.out.println("");//creates new line
	}			


}
public static int check(int input, int count, int[] numUsed) {
	Scanner sc = new Scanner(System.in);//initialize scanner
	boolean used = false;// boolean for if number has been used

boolean bigLoop = false; //automatically run big loop in checking for input

//do a linear search to check if input has been used already
for (int i = 0; i < 9; i++) {
	if (input == numUsed[i]) {
		used = true;
	}
}
numUsed[count] = input; //assign a number in an array to input

if (input < 10 && input > 0 && used == false) {
	return input; //if number has not been used and is in range, return to main method
}

	
while (bigLoop == false) {
	used = false; //resent used to false
	System.out.println("Please enter an int between 1 and 9 that has not been used");
	while (!sc.hasNextInt()) { //checking if the computer did not recieve an int input
	        System.out.println("That's not an int! Try again!");//informing user of their silly error
	     sc.next(); //removing error from input
	    }	
input = sc.nextInt(); //assign input to answer

//do a linear search to check if input has been used already
for (int i = 0; i < 9; i++) {
	if (input == numUsed[i]) {
		used = true; //assign used to true if number is found
	}
}
numUsed[count] = input;
if (input < 10 && input > 0 && used == false) {
	return input; //if number has not been used and is in range, return to main method
}
	
}
	

return input;	//return to main method
}

public static void userInput(int input, char[][] grid, char team){

//this method assigns the correct team to the respective input
if (input == 1) {
grid[0][0] = team;
}
else if (input == 2) {
	grid[0][1] = team;
}
else if (input == 3) {
	grid[0][2] = team;
}
else if (input == 4) {
	grid[1][0] = team;
}
else if (input == 5) {
	grid[1][1] = team;
}
else if (input == 6) {
	grid[1][2] = team;
}
else if (input == 7) {
	grid[2][0] = team;
}
else if (input == 8) {
	grid[2][1] = team;
}
else if (input == 9) {
	grid[2][2] = team;
}


print(grid); //calls print method

}

public static void win(char[][] grid, int count) {

//method is used to check for a winner and is called after every move in the main method

//each if statement will check to see if the numbers in the same row, column or diagnal are the same
	//if they are, it will print a win statement along with which team one
	//then it will exit the program
if (grid[0][0] == grid[0][1] && grid[0][2] == grid[0][1]) {
	System.out.println("Team " + grid[0][0] + " wins!");
	System.exit(0);
}

else if (grid[1][0] == grid[1][1] && grid[1][2] == grid[1][1]) {
System.out.println("Team " + grid[1][0] + " wins!");
System.exit(0);
}

else if (grid[2][0] == grid[2][1] && grid[2][2] == grid[2][1]) {
System.out.println("Team " + grid[2][0] + " wins!");
System.exit(0);
}

else if (grid[0][0] == grid[1][0] && grid[1][0] == grid[2][0]) {
System.out.println("Team " + grid[2][0] + " wins!");
System.exit(0);
}

else if (grid[0][1] == grid[1][1] && grid[1][1] == grid[2][1]) {
System.out.println("Team " + grid[2][1] + " wins!");
System.exit(0);
}

else if (grid[0][2] == grid[1][2] && grid[1][2] == grid[2][2]) {
System.out.println("Team " + grid[2][2] + " wins!");
System.exit(0);
}

else if (grid[0][0] == grid[1][1] && grid[1][1] == grid[2][2]) {
System.out.println("Team " + grid[2][2] + " wins!");
System.exit(0);
}

else if (grid[0][2] == grid[1][1] && grid[1][1] == grid[2][0]) {
System.out.println("Team " + grid[2][0] + " wins!");
System.exit(0);
}

//if the move variable count has reached 8 (meaning 9 moves) and a winner has not been found, call a draw
else if (count == 8) {
	System.out.println("Draw");
	System.exit(0);
}

	
}

}
